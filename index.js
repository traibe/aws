/**
    --------------------------------------------------------------------------------
    
    ooooooooooooo ooooooooo.         .o.       ooooo oooooooooo.  oooooooooooo 
    8'   888   `8 `888   `Y88.      .888.      `888' `888'   `Y8b `888'     `8 
         888       888   .d88'     .8"888.      888   888     888  888         
         888       888ooo88P'     .8' `888.     888   888oooo888'  888oooo8    
         888       888`88b.      .88ooo8888.    888   888    `88b  888    "    
         888       888  `88b.   .8'     `888.   888   888    .88P  888       o 
        o888o     o888o  o888o o88o     o8888o o888o o888bood8P'  o888ooooood8                                                                        
                                                                          
    --------------------------------------------------------------------------------
    AWS SDK for Traibe
    --------------------------------------------------------------------------------

    Author:     Nicholas E. Hamilton, PhD.
    Email:      nicholasehamilton@gmail.com
    Date:       21st March 2021
*/
const AWS       = require('aws-sdk');
let impotent    = false;

// Environmental Variables
const {AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_SES_REGION} = process.env;
if(!AWS_ACCESS_KEY_ID || !AWS_SECRET_ACCESS_KEY || !AWS_SES_REGION){
    impotent = true;
    console.warn(
        `AWS_ACCESS_KEY_ID (${AWS_ACCESS_KEY_ID}), AWS_SECRET_ACCESS_KEY (${AWS_SECRET_ACCESS_KEY}) and AWS_SES_REGION (${AWS_SES_REGION}) must be set as environmental variables, send email will be impotent`
    )
}

AWS.config.update({
    accessKeyId:        AWS_ACCESS_KEY_ID,
    secretAccessKey:    AWS_SECRET_ACCESS_KEY,
    region:             AWS_SES_REGION
});

module.exports = {
    AWS, 
    impotent
}